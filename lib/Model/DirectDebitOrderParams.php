<?php
/**
 * DirectDebitOrderParams
 *
 * PHP version 5
 *
 * @category Class
 * @package  Finapi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * finAPI Access
 *
 * <strong>RESTful API for Account Information Services (AIS) and Payment Initiation Services (PIS)</strong>  The following pages give you some general information on how to use our APIs.<br/> The actual API services documentation then follows further below. You can use the menu to jump between API sections. <br/> <br/> This page has a built-in HTTP(S) client, so you can test the services directly from within this page, by filling in the request parameters and/or body in the respective services, and then hitting the TRY button. Note that you need to be authorized to make a successful API call. To authorize, refer to the 'Authorization' section of the API, or just use the OAUTH button that can be found near the TRY button. <br/> <br/> You should also check out the <a href=\"https://finapi.zendesk.com/hc/en-us\" target=\"_blank\">Developer Portal</a> for more information. If you need any help with the API, contact support@finapi.io. <br/>  <h2>General information</h2>  <h3><strong>Error Responses</strong></h3> When an API call returns with an error, then in general it has the structure shown in the following example:  <pre> {   \"errors\": [     {       \"message\": \"Interface 'FINTS_SERVER' is not supported for this operation.\",       \"code\": \"BAD_REQUEST\",       \"type\": \"TECHNICAL\"     }   ],   \"date\": \"2020-11-19 16:54:06.854\",   \"requestId\": \"selfgen-312042e7-df55-47e4-bffd-956a68ef37b5\",   \"endpoint\": \"POST /api/v1/bankConnections/import\",   \"authContext\": \"1/21\",   \"bank\": \"DEMO0002 - finAPI Test Redirect Bank\" } </pre>  If an API call requires an additional authentication by the user, HTTP code 510 is returned and the error response contains the additional \"multiStepAuthentication\" object, see the following example:  <pre> {   \"errors\": [     {       \"message\": \"Es ist eine zusätzliche Authentifizierung erforderlich. Bitte geben Sie folgenden Code an: 123456\",       \"code\": \"ADDITIONAL_AUTHENTICATION_REQUIRED\",       \"type\": \"BUSINESS\",       \"multiStepAuthentication\": {         \"hash\": \"678b13f4be9ed7d981a840af8131223a\",         \"status\": \"CHALLENGE_RESPONSE_REQUIRED\",         \"challengeMessage\": \"Es ist eine zusätzliche Authentifizierung erforderlich. Bitte geben Sie folgenden Code an: 123456\",         \"answerFieldLabel\": \"TAN\",         \"redirectUrl\": null,         \"redirectContext\": null,         \"redirectContextField\": null,         \"twoStepProcedures\": null,         \"photoTanMimeType\": null,         \"photoTanData\": null,         \"opticalData\": null       }     }   ],   \"date\": \"2019-11-29 09:51:55.931\",   \"requestId\": \"selfgen-45059c99-1b14-4df7-9bd3-9d5f126df294\",   \"endpoint\": \"POST /api/v1/bankConnections/import\",   \"authContext\": \"1/18\",   \"bank\": \"DEMO0001 - finAPI Test Bank\" } </pre>  An exception to this error format are API authentication errors, where the following structure is returned:  <pre> {   \"error\": \"invalid_token\",   \"error_description\": \"Invalid access token: cccbce46-xxxx-xxxx-xxxx-xxxxxxxxxx\" } </pre>  <h3><strong>Paging</strong></h3> API services that may potentially return a lot of data implement paging. They return a limited number of entries within a \"page\". Further entries must be fetched with subsequent calls. <br/><br/> Any API service that implements paging provides the following input parameters:<br/> &bull; \"page\": the number of the page to be retrieved (starting with 1).<br/> &bull; \"perPage\": the number of entries within a page. The default and maximum value is stated in the documentation of the respective services.  A paged response contains an additional \"paging\" object with the following structure:  <pre> {   ...   ,   \"paging\": {     \"page\": 1,     \"perPage\": 20,     \"pageCount\": 234,     \"totalCount\": 4662   } } </pre>  <h3><strong>Internationalization</strong></h3> The finAPI services support internationalization which means you can define the language you prefer for API service responses. <br/><br/> The following languages are available: German, English, Czech, Slovak. <br/><br/> The preferred language can be defined by providing the official HTTP <strong>Accept-Language</strong> header. For web form request issued in a web browser, the Accept-Language header is automatically set by the browser based on the browser's or operation system's language settings. For direct API calls, the Accept-Language header must be set explicity. <br/><br/> finAPI reacts on the official iso language codes &quot;de&quot;, &quot;en&quot;, &quot;cs&quot; and &quot;sk&quot; for the named languages. Additional subtags supported by the Accept-Language header may be provided, e.g. &quot;en-US&quot;, but are ignored. <br/> If no Accept-Language header is given, German is used as the default language. <br/><br/> Exceptions:<br/> &bull; Bank login hints and login fields are only available in the language of the bank and not being translated.<br/> &bull; Direct messages from the bank systems typically returned as BUSINESS errors will not be translated.<br/> &bull; BUSINESS errors created by finAPI directly are available in German and English.<br/> &bull; TECHNICAL errors messages meant for developers are mostly in English, but also may be translated.  <h3><strong>Request IDs</strong></h3> With any API call, you can pass a request ID via a header with name \"X-REQUEST-ID\". The request ID can be an arbitrary string with up to 255 characters. Passing a longer string will result in an error. <br/><br/> If you don't pass a request ID for a call, finAPI will generate a random ID internally. <br/><br/> The request ID is always returned back in the response of a service, as a header with name \"X-REQUEST-ID\". <br/><br/> We highly recommend to always pass a (preferably unique) request ID, and include it into your client application logs whenever you make a request or receive a response (especially in the case of an error response). finAPI is also logging request IDs on its end. Having a request ID can help the finAPI support team to work more efficiently and solve tickets faster.  <h3><strong>Overriding HTTP methods</strong></h3> Some HTTP clients do not support the HTTP methods PATCH or DELETE. If you are using such a client in your application, you can use a POST request instead with a special HTTP header indicating the originally intended HTTP method. <br/><br/> The header's name is <strong>X-HTTP-Method-Override</strong>. Set its value to either <strong>PATCH</strong> or <strong>DELETE</strong>. POST Requests having this header set will be treated either as PATCH or DELETE by the finAPI servers. <br/><br/> Example: <br/><br/> <strong>X-HTTP-Method-Override: PATCH</strong><br/> POST /api/v1/label/51<br/> {\"name\": \"changed label\"}<br/><br/> will be interpreted by finAPI as:<br/><br/> PATCH /api/v1/label/51<br/> {\"name\": \"changed label\"}<br/>  <h3><strong>User metadata</strong></h3> With the migration to PSD2 APIs, a new term called \"User metadata\" (also known as \"PSU metadata\") has been introduced to the API. This user metadata aims to inform the banking API if there was a real end-user behind an HTTP request or if the request was triggered by a system (e.g. by an automatic batch update). In the latter case, the bank may apply some restrictions such as limiting the number of HTTP requests for a single consent. Also, some operations may be forbidden entirely by the banking API. For example, some banks do not allow issuing a new consent without the end-user being involved. Therefore, the PSU metadata must always be provided for such operations. <br/><br/> As finAPI does not have direct interaction with the end-user, it is the client application's responsibility to provide all the necessary information about the end-user. This must be done by sending additional headers with every request triggered on behalf of the end-user. <br/><br/> At the moment, the following headers are supported by the API:<br/> &bull; \"PSU-IP-Address\" - the IP address of the user's device.<br/> &bull; \"PSU-Device-OS\" - the user's device and/or operating system identification.<br/> &bull; \"PSU-User-Agent\" - the user's web browser or other client device identification. <br/><br/> Web-form customers (or unlicensed customers) must send the PSU headers from their client application to finAPI. It will not take effect if web form is triggered for the workflow. <br/> In this case Values for the PSU-Device-OS and PSU-User-Agent headers are identified by the JS platform detection and the PSU-IP-Address is obtained from a public Cloudflare service: https://www.cloudflare.com/cdn-cgi/trace. <br/><br/> But it is certainly necessary and obligatory to have the true PSU header data for API calls which don't trigger a web form (like \"Update a bank connection\").  <h3><strong>FAQ</strong></h3> <strong>Is there a finAPI SDK?</strong> <br/> Currently we do not offer a native SDK, but there is the option to generate a SDK for almost any target language via Swagger. Use the 'Download SDK' button on this page for SDK generation. <br/> <br/> <strong>How can I enable finAPI's automatic batch update?</strong> <br/> Currently there is no way to set up the batch update via the API. Please contact support@finapi.io for this. <br/> <br/> <strong>Why do I need to keep authorizing when calling services on this page?</strong> <br/> This page is a \"one-page-app\". Reloading the page resets the OAuth authorization context. There is generally no need to reload the page, so just don't do it and your authorization will persist.
 *
 * OpenAPI spec version: 1.121.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.18
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Finapi\Model;

use \ArrayAccess;
use \Finapi\ObjectSerializer;

/**
 * DirectDebitOrderParams Class Doc Comment
 *
 * @category Class
 * @description Parameters for a direct debit order
 * @package  Finapi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class DirectDebitOrderParams implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'DirectDebitOrderParams';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'counterpart_name' => 'string',
        'counterpart_iban' => 'string',
        'counterpart_bic' => 'string',
        'amount' => 'float',
        'purpose' => 'string',
        'sepa_purpose_code' => 'string',
        'end_to_end_id' => 'string',
        'mandate_id' => 'string',
        'mandate_date' => 'string',
        'creditor_id' => 'string',
        'counterpart_address' => 'string',
        'counterpart_country' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'counterpart_name' => null,
        'counterpart_iban' => null,
        'counterpart_bic' => null,
        'amount' => null,
        'purpose' => null,
        'sepa_purpose_code' => null,
        'end_to_end_id' => null,
        'mandate_id' => null,
        'mandate_date' => null,
        'creditor_id' => null,
        'counterpart_address' => null,
        'counterpart_country' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'counterpart_name' => 'counterpartName',
        'counterpart_iban' => 'counterpartIban',
        'counterpart_bic' => 'counterpartBic',
        'amount' => 'amount',
        'purpose' => 'purpose',
        'sepa_purpose_code' => 'sepaPurposeCode',
        'end_to_end_id' => 'endToEndId',
        'mandate_id' => 'mandateId',
        'mandate_date' => 'mandateDate',
        'creditor_id' => 'creditorId',
        'counterpart_address' => 'counterpartAddress',
        'counterpart_country' => 'counterpartCountry'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'counterpart_name' => 'setCounterpartName',
        'counterpart_iban' => 'setCounterpartIban',
        'counterpart_bic' => 'setCounterpartBic',
        'amount' => 'setAmount',
        'purpose' => 'setPurpose',
        'sepa_purpose_code' => 'setSepaPurposeCode',
        'end_to_end_id' => 'setEndToEndId',
        'mandate_id' => 'setMandateId',
        'mandate_date' => 'setMandateDate',
        'creditor_id' => 'setCreditorId',
        'counterpart_address' => 'setCounterpartAddress',
        'counterpart_country' => 'setCounterpartCountry'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'counterpart_name' => 'getCounterpartName',
        'counterpart_iban' => 'getCounterpartIban',
        'counterpart_bic' => 'getCounterpartBic',
        'amount' => 'getAmount',
        'purpose' => 'getPurpose',
        'sepa_purpose_code' => 'getSepaPurposeCode',
        'end_to_end_id' => 'getEndToEndId',
        'mandate_id' => 'getMandateId',
        'mandate_date' => 'getMandateDate',
        'creditor_id' => 'getCreditorId',
        'counterpart_address' => 'getCounterpartAddress',
        'counterpart_country' => 'getCounterpartCountry'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const COUNTERPART_COUNTRY_AD = 'AD';
    const COUNTERPART_COUNTRY_AE = 'AE';
    const COUNTERPART_COUNTRY_AF = 'AF';
    const COUNTERPART_COUNTRY_AG = 'AG';
    const COUNTERPART_COUNTRY_AI = 'AI';
    const COUNTERPART_COUNTRY_AL = 'AL';
    const COUNTERPART_COUNTRY_AM = 'AM';
    const COUNTERPART_COUNTRY_AO = 'AO';
    const COUNTERPART_COUNTRY_AQ = 'AQ';
    const COUNTERPART_COUNTRY_AR = 'AR';
    const COUNTERPART_COUNTRY__AS = 'AS';
    const COUNTERPART_COUNTRY_AT = 'AT';
    const COUNTERPART_COUNTRY_AU = 'AU';
    const COUNTERPART_COUNTRY_AW = 'AW';
    const COUNTERPART_COUNTRY_AX = 'AX';
    const COUNTERPART_COUNTRY_AZ = 'AZ';
    const COUNTERPART_COUNTRY_BA = 'BA';
    const COUNTERPART_COUNTRY_BB = 'BB';
    const COUNTERPART_COUNTRY_BD = 'BD';
    const COUNTERPART_COUNTRY_BE = 'BE';
    const COUNTERPART_COUNTRY_BF = 'BF';
    const COUNTERPART_COUNTRY_BG = 'BG';
    const COUNTERPART_COUNTRY_BH = 'BH';
    const COUNTERPART_COUNTRY_BI = 'BI';
    const COUNTERPART_COUNTRY_BJ = 'BJ';
    const COUNTERPART_COUNTRY_BL = 'BL';
    const COUNTERPART_COUNTRY_BM = 'BM';
    const COUNTERPART_COUNTRY_BN = 'BN';
    const COUNTERPART_COUNTRY_BO = 'BO';
    const COUNTERPART_COUNTRY_BQ = 'BQ';
    const COUNTERPART_COUNTRY_BR = 'BR';
    const COUNTERPART_COUNTRY_BS = 'BS';
    const COUNTERPART_COUNTRY_BT = 'BT';
    const COUNTERPART_COUNTRY_BV = 'BV';
    const COUNTERPART_COUNTRY_BW = 'BW';
    const COUNTERPART_COUNTRY_BY = 'BY';
    const COUNTERPART_COUNTRY_BZ = 'BZ';
    const COUNTERPART_COUNTRY_CA = 'CA';
    const COUNTERPART_COUNTRY_CC = 'CC';
    const COUNTERPART_COUNTRY_CD = 'CD';
    const COUNTERPART_COUNTRY_CF = 'CF';
    const COUNTERPART_COUNTRY_CG = 'CG';
    const COUNTERPART_COUNTRY_CH = 'CH';
    const COUNTERPART_COUNTRY_CI = 'CI';
    const COUNTERPART_COUNTRY_CK = 'CK';
    const COUNTERPART_COUNTRY_CL = 'CL';
    const COUNTERPART_COUNTRY_CM = 'CM';
    const COUNTERPART_COUNTRY_CN = 'CN';
    const COUNTERPART_COUNTRY_CO = 'CO';
    const COUNTERPART_COUNTRY_CR = 'CR';
    const COUNTERPART_COUNTRY_CU = 'CU';
    const COUNTERPART_COUNTRY_CV = 'CV';
    const COUNTERPART_COUNTRY_CW = 'CW';
    const COUNTERPART_COUNTRY_CX = 'CX';
    const COUNTERPART_COUNTRY_CY = 'CY';
    const COUNTERPART_COUNTRY_CZ = 'CZ';
    const COUNTERPART_COUNTRY_DE = 'DE';
    const COUNTERPART_COUNTRY_DJ = 'DJ';
    const COUNTERPART_COUNTRY_DK = 'DK';
    const COUNTERPART_COUNTRY_DM = 'DM';
    const COUNTERPART_COUNTRY__DO = 'DO';
    const COUNTERPART_COUNTRY_DZ = 'DZ';
    const COUNTERPART_COUNTRY_EC = 'EC';
    const COUNTERPART_COUNTRY_EE = 'EE';
    const COUNTERPART_COUNTRY_EG = 'EG';
    const COUNTERPART_COUNTRY_EH = 'EH';
    const COUNTERPART_COUNTRY_ER = 'ER';
    const COUNTERPART_COUNTRY_ES = 'ES';
    const COUNTERPART_COUNTRY_ET = 'ET';
    const COUNTERPART_COUNTRY_FI = 'FI';
    const COUNTERPART_COUNTRY_FJ = 'FJ';
    const COUNTERPART_COUNTRY_FK = 'FK';
    const COUNTERPART_COUNTRY_FM = 'FM';
    const COUNTERPART_COUNTRY_FO = 'FO';
    const COUNTERPART_COUNTRY_FR = 'FR';
    const COUNTERPART_COUNTRY_GA = 'GA';
    const COUNTERPART_COUNTRY_GB = 'GB';
    const COUNTERPART_COUNTRY_GD = 'GD';
    const COUNTERPART_COUNTRY_GE = 'GE';
    const COUNTERPART_COUNTRY_GF = 'GF';
    const COUNTERPART_COUNTRY_GG = 'GG';
    const COUNTERPART_COUNTRY_GH = 'GH';
    const COUNTERPART_COUNTRY_GI = 'GI';
    const COUNTERPART_COUNTRY_GL = 'GL';
    const COUNTERPART_COUNTRY_GM = 'GM';
    const COUNTERPART_COUNTRY_GN = 'GN';
    const COUNTERPART_COUNTRY_GP = 'GP';
    const COUNTERPART_COUNTRY_GQ = 'GQ';
    const COUNTERPART_COUNTRY_GR = 'GR';
    const COUNTERPART_COUNTRY_GS = 'GS';
    const COUNTERPART_COUNTRY_GT = 'GT';
    const COUNTERPART_COUNTRY_GU = 'GU';
    const COUNTERPART_COUNTRY_GW = 'GW';
    const COUNTERPART_COUNTRY_GY = 'GY';
    const COUNTERPART_COUNTRY_HK = 'HK';
    const COUNTERPART_COUNTRY_HM = 'HM';
    const COUNTERPART_COUNTRY_HN = 'HN';
    const COUNTERPART_COUNTRY_HR = 'HR';
    const COUNTERPART_COUNTRY_HT = 'HT';
    const COUNTERPART_COUNTRY_HU = 'HU';
    const COUNTERPART_COUNTRY_ID = 'ID';
    const COUNTERPART_COUNTRY_IE = 'IE';
    const COUNTERPART_COUNTRY_IL = 'IL';
    const COUNTERPART_COUNTRY_IM = 'IM';
    const COUNTERPART_COUNTRY_IN = 'IN';
    const COUNTERPART_COUNTRY_IO = 'IO';
    const COUNTERPART_COUNTRY_IQ = 'IQ';
    const COUNTERPART_COUNTRY_IR = 'IR';
    const COUNTERPART_COUNTRY_IS = 'IS';
    const COUNTERPART_COUNTRY_IT = 'IT';
    const COUNTERPART_COUNTRY_JE = 'JE';
    const COUNTERPART_COUNTRY_JM = 'JM';
    const COUNTERPART_COUNTRY_JO = 'JO';
    const COUNTERPART_COUNTRY_JP = 'JP';
    const COUNTERPART_COUNTRY_KE = 'KE';
    const COUNTERPART_COUNTRY_KG = 'KG';
    const COUNTERPART_COUNTRY_KH = 'KH';
    const COUNTERPART_COUNTRY_KI = 'KI';
    const COUNTERPART_COUNTRY_KM = 'KM';
    const COUNTERPART_COUNTRY_KN = 'KN';
    const COUNTERPART_COUNTRY_KP = 'KP';
    const COUNTERPART_COUNTRY_KR = 'KR';
    const COUNTERPART_COUNTRY_KW = 'KW';
    const COUNTERPART_COUNTRY_KY = 'KY';
    const COUNTERPART_COUNTRY_KZ = 'KZ';
    const COUNTERPART_COUNTRY_LA = 'LA';
    const COUNTERPART_COUNTRY_LB = 'LB';
    const COUNTERPART_COUNTRY_LC = 'LC';
    const COUNTERPART_COUNTRY_LI = 'LI';
    const COUNTERPART_COUNTRY_LK = 'LK';
    const COUNTERPART_COUNTRY_LR = 'LR';
    const COUNTERPART_COUNTRY_LS = 'LS';
    const COUNTERPART_COUNTRY_LT = 'LT';
    const COUNTERPART_COUNTRY_LU = 'LU';
    const COUNTERPART_COUNTRY_LV = 'LV';
    const COUNTERPART_COUNTRY_LY = 'LY';
    const COUNTERPART_COUNTRY_MA = 'MA';
    const COUNTERPART_COUNTRY_MC = 'MC';
    const COUNTERPART_COUNTRY_MD = 'MD';
    const COUNTERPART_COUNTRY_ME = 'ME';
    const COUNTERPART_COUNTRY_MF = 'MF';
    const COUNTERPART_COUNTRY_MG = 'MG';
    const COUNTERPART_COUNTRY_MH = 'MH';
    const COUNTERPART_COUNTRY_MK = 'MK';
    const COUNTERPART_COUNTRY_ML = 'ML';
    const COUNTERPART_COUNTRY_MM = 'MM';
    const COUNTERPART_COUNTRY_MN = 'MN';
    const COUNTERPART_COUNTRY_MO = 'MO';
    const COUNTERPART_COUNTRY_MP = 'MP';
    const COUNTERPART_COUNTRY_MQ = 'MQ';
    const COUNTERPART_COUNTRY_MR = 'MR';
    const COUNTERPART_COUNTRY_MS = 'MS';
    const COUNTERPART_COUNTRY_MT = 'MT';
    const COUNTERPART_COUNTRY_MU = 'MU';
    const COUNTERPART_COUNTRY_MV = 'MV';
    const COUNTERPART_COUNTRY_MW = 'MW';
    const COUNTERPART_COUNTRY_MX = 'MX';
    const COUNTERPART_COUNTRY_MY = 'MY';
    const COUNTERPART_COUNTRY_MZ = 'MZ';
    const COUNTERPART_COUNTRY_NA = 'NA';
    const COUNTERPART_COUNTRY_NC = 'NC';
    const COUNTERPART_COUNTRY_NE = 'NE';
    const COUNTERPART_COUNTRY_NF = 'NF';
    const COUNTERPART_COUNTRY_NG = 'NG';
    const COUNTERPART_COUNTRY_NI = 'NI';
    const COUNTERPART_COUNTRY_NL = 'NL';
    const COUNTERPART_COUNTRY_NO = 'NO';
    const COUNTERPART_COUNTRY_NP = 'NP';
    const COUNTERPART_COUNTRY_NR = 'NR';
    const COUNTERPART_COUNTRY_NU = 'NU';
    const COUNTERPART_COUNTRY_NZ = 'NZ';
    const COUNTERPART_COUNTRY_OM = 'OM';
    const COUNTERPART_COUNTRY_PA = 'PA';
    const COUNTERPART_COUNTRY_PE = 'PE';
    const COUNTERPART_COUNTRY_PF = 'PF';
    const COUNTERPART_COUNTRY_PG = 'PG';
    const COUNTERPART_COUNTRY_PH = 'PH';
    const COUNTERPART_COUNTRY_PK = 'PK';
    const COUNTERPART_COUNTRY_PL = 'PL';
    const COUNTERPART_COUNTRY_PM = 'PM';
    const COUNTERPART_COUNTRY_PN = 'PN';
    const COUNTERPART_COUNTRY_PR = 'PR';
    const COUNTERPART_COUNTRY_PS = 'PS';
    const COUNTERPART_COUNTRY_PT = 'PT';
    const COUNTERPART_COUNTRY_PW = 'PW';
    const COUNTERPART_COUNTRY_PY = 'PY';
    const COUNTERPART_COUNTRY_QA = 'QA';
    const COUNTERPART_COUNTRY_RE = 'RE';
    const COUNTERPART_COUNTRY_RO = 'RO';
    const COUNTERPART_COUNTRY_RS = 'RS';
    const COUNTERPART_COUNTRY_RU = 'RU';
    const COUNTERPART_COUNTRY_RW = 'RW';
    const COUNTERPART_COUNTRY_SA = 'SA';
    const COUNTERPART_COUNTRY_SB = 'SB';
    const COUNTERPART_COUNTRY_SC = 'SC';
    const COUNTERPART_COUNTRY_SD = 'SD';
    const COUNTERPART_COUNTRY_SE = 'SE';
    const COUNTERPART_COUNTRY_SG = 'SG';
    const COUNTERPART_COUNTRY_SH = 'SH';
    const COUNTERPART_COUNTRY_SI = 'SI';
    const COUNTERPART_COUNTRY_SJ = 'SJ';
    const COUNTERPART_COUNTRY_SK = 'SK';
    const COUNTERPART_COUNTRY_SL = 'SL';
    const COUNTERPART_COUNTRY_SM = 'SM';
    const COUNTERPART_COUNTRY_SN = 'SN';
    const COUNTERPART_COUNTRY_SO = 'SO';
    const COUNTERPART_COUNTRY_SR = 'SR';
    const COUNTERPART_COUNTRY_SS = 'SS';
    const COUNTERPART_COUNTRY_ST = 'ST';
    const COUNTERPART_COUNTRY_SV = 'SV';
    const COUNTERPART_COUNTRY_SX = 'SX';
    const COUNTERPART_COUNTRY_SY = 'SY';
    const COUNTERPART_COUNTRY_SZ = 'SZ';
    const COUNTERPART_COUNTRY_TC = 'TC';
    const COUNTERPART_COUNTRY_TD = 'TD';
    const COUNTERPART_COUNTRY_TF = 'TF';
    const COUNTERPART_COUNTRY_TG = 'TG';
    const COUNTERPART_COUNTRY_TH = 'TH';
    const COUNTERPART_COUNTRY_TJ = 'TJ';
    const COUNTERPART_COUNTRY_TK = 'TK';
    const COUNTERPART_COUNTRY_TL = 'TL';
    const COUNTERPART_COUNTRY_TM = 'TM';
    const COUNTERPART_COUNTRY_TN = 'TN';
    const COUNTERPART_COUNTRY_TO = 'TO';
    const COUNTERPART_COUNTRY_TR = 'TR';
    const COUNTERPART_COUNTRY_TT = 'TT';
    const COUNTERPART_COUNTRY_TV = 'TV';
    const COUNTERPART_COUNTRY_TW = 'TW';
    const COUNTERPART_COUNTRY_TZ = 'TZ';
    const COUNTERPART_COUNTRY_UA = 'UA';
    const COUNTERPART_COUNTRY_UG = 'UG';
    const COUNTERPART_COUNTRY_UM = 'UM';
    const COUNTERPART_COUNTRY_US = 'US';
    const COUNTERPART_COUNTRY_UY = 'UY';
    const COUNTERPART_COUNTRY_UZ = 'UZ';
    const COUNTERPART_COUNTRY_VA = 'VA';
    const COUNTERPART_COUNTRY_VC = 'VC';
    const COUNTERPART_COUNTRY_VE = 'VE';
    const COUNTERPART_COUNTRY_VG = 'VG';
    const COUNTERPART_COUNTRY_VI = 'VI';
    const COUNTERPART_COUNTRY_VN = 'VN';
    const COUNTERPART_COUNTRY_VU = 'VU';
    const COUNTERPART_COUNTRY_WF = 'WF';
    const COUNTERPART_COUNTRY_WS = 'WS';
    const COUNTERPART_COUNTRY_XK = 'XK';
    const COUNTERPART_COUNTRY_YE = 'YE';
    const COUNTERPART_COUNTRY_YT = 'YT';
    const COUNTERPART_COUNTRY_ZA = 'ZA';
    const COUNTERPART_COUNTRY_ZM = 'ZM';
    const COUNTERPART_COUNTRY_ZW = 'ZW';
    

    
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getCounterpartCountryAllowableValues()
    {
        return [
            self::COUNTERPART_COUNTRY_AD,
            self::COUNTERPART_COUNTRY_AE,
            self::COUNTERPART_COUNTRY_AF,
            self::COUNTERPART_COUNTRY_AG,
            self::COUNTERPART_COUNTRY_AI,
            self::COUNTERPART_COUNTRY_AL,
            self::COUNTERPART_COUNTRY_AM,
            self::COUNTERPART_COUNTRY_AO,
            self::COUNTERPART_COUNTRY_AQ,
            self::COUNTERPART_COUNTRY_AR,
            self::COUNTERPART_COUNTRY__AS,
            self::COUNTERPART_COUNTRY_AT,
            self::COUNTERPART_COUNTRY_AU,
            self::COUNTERPART_COUNTRY_AW,
            self::COUNTERPART_COUNTRY_AX,
            self::COUNTERPART_COUNTRY_AZ,
            self::COUNTERPART_COUNTRY_BA,
            self::COUNTERPART_COUNTRY_BB,
            self::COUNTERPART_COUNTRY_BD,
            self::COUNTERPART_COUNTRY_BE,
            self::COUNTERPART_COUNTRY_BF,
            self::COUNTERPART_COUNTRY_BG,
            self::COUNTERPART_COUNTRY_BH,
            self::COUNTERPART_COUNTRY_BI,
            self::COUNTERPART_COUNTRY_BJ,
            self::COUNTERPART_COUNTRY_BL,
            self::COUNTERPART_COUNTRY_BM,
            self::COUNTERPART_COUNTRY_BN,
            self::COUNTERPART_COUNTRY_BO,
            self::COUNTERPART_COUNTRY_BQ,
            self::COUNTERPART_COUNTRY_BR,
            self::COUNTERPART_COUNTRY_BS,
            self::COUNTERPART_COUNTRY_BT,
            self::COUNTERPART_COUNTRY_BV,
            self::COUNTERPART_COUNTRY_BW,
            self::COUNTERPART_COUNTRY_BY,
            self::COUNTERPART_COUNTRY_BZ,
            self::COUNTERPART_COUNTRY_CA,
            self::COUNTERPART_COUNTRY_CC,
            self::COUNTERPART_COUNTRY_CD,
            self::COUNTERPART_COUNTRY_CF,
            self::COUNTERPART_COUNTRY_CG,
            self::COUNTERPART_COUNTRY_CH,
            self::COUNTERPART_COUNTRY_CI,
            self::COUNTERPART_COUNTRY_CK,
            self::COUNTERPART_COUNTRY_CL,
            self::COUNTERPART_COUNTRY_CM,
            self::COUNTERPART_COUNTRY_CN,
            self::COUNTERPART_COUNTRY_CO,
            self::COUNTERPART_COUNTRY_CR,
            self::COUNTERPART_COUNTRY_CU,
            self::COUNTERPART_COUNTRY_CV,
            self::COUNTERPART_COUNTRY_CW,
            self::COUNTERPART_COUNTRY_CX,
            self::COUNTERPART_COUNTRY_CY,
            self::COUNTERPART_COUNTRY_CZ,
            self::COUNTERPART_COUNTRY_DE,
            self::COUNTERPART_COUNTRY_DJ,
            self::COUNTERPART_COUNTRY_DK,
            self::COUNTERPART_COUNTRY_DM,
            self::COUNTERPART_COUNTRY__DO,
            self::COUNTERPART_COUNTRY_DZ,
            self::COUNTERPART_COUNTRY_EC,
            self::COUNTERPART_COUNTRY_EE,
            self::COUNTERPART_COUNTRY_EG,
            self::COUNTERPART_COUNTRY_EH,
            self::COUNTERPART_COUNTRY_ER,
            self::COUNTERPART_COUNTRY_ES,
            self::COUNTERPART_COUNTRY_ET,
            self::COUNTERPART_COUNTRY_FI,
            self::COUNTERPART_COUNTRY_FJ,
            self::COUNTERPART_COUNTRY_FK,
            self::COUNTERPART_COUNTRY_FM,
            self::COUNTERPART_COUNTRY_FO,
            self::COUNTERPART_COUNTRY_FR,
            self::COUNTERPART_COUNTRY_GA,
            self::COUNTERPART_COUNTRY_GB,
            self::COUNTERPART_COUNTRY_GD,
            self::COUNTERPART_COUNTRY_GE,
            self::COUNTERPART_COUNTRY_GF,
            self::COUNTERPART_COUNTRY_GG,
            self::COUNTERPART_COUNTRY_GH,
            self::COUNTERPART_COUNTRY_GI,
            self::COUNTERPART_COUNTRY_GL,
            self::COUNTERPART_COUNTRY_GM,
            self::COUNTERPART_COUNTRY_GN,
            self::COUNTERPART_COUNTRY_GP,
            self::COUNTERPART_COUNTRY_GQ,
            self::COUNTERPART_COUNTRY_GR,
            self::COUNTERPART_COUNTRY_GS,
            self::COUNTERPART_COUNTRY_GT,
            self::COUNTERPART_COUNTRY_GU,
            self::COUNTERPART_COUNTRY_GW,
            self::COUNTERPART_COUNTRY_GY,
            self::COUNTERPART_COUNTRY_HK,
            self::COUNTERPART_COUNTRY_HM,
            self::COUNTERPART_COUNTRY_HN,
            self::COUNTERPART_COUNTRY_HR,
            self::COUNTERPART_COUNTRY_HT,
            self::COUNTERPART_COUNTRY_HU,
            self::COUNTERPART_COUNTRY_ID,
            self::COUNTERPART_COUNTRY_IE,
            self::COUNTERPART_COUNTRY_IL,
            self::COUNTERPART_COUNTRY_IM,
            self::COUNTERPART_COUNTRY_IN,
            self::COUNTERPART_COUNTRY_IO,
            self::COUNTERPART_COUNTRY_IQ,
            self::COUNTERPART_COUNTRY_IR,
            self::COUNTERPART_COUNTRY_IS,
            self::COUNTERPART_COUNTRY_IT,
            self::COUNTERPART_COUNTRY_JE,
            self::COUNTERPART_COUNTRY_JM,
            self::COUNTERPART_COUNTRY_JO,
            self::COUNTERPART_COUNTRY_JP,
            self::COUNTERPART_COUNTRY_KE,
            self::COUNTERPART_COUNTRY_KG,
            self::COUNTERPART_COUNTRY_KH,
            self::COUNTERPART_COUNTRY_KI,
            self::COUNTERPART_COUNTRY_KM,
            self::COUNTERPART_COUNTRY_KN,
            self::COUNTERPART_COUNTRY_KP,
            self::COUNTERPART_COUNTRY_KR,
            self::COUNTERPART_COUNTRY_KW,
            self::COUNTERPART_COUNTRY_KY,
            self::COUNTERPART_COUNTRY_KZ,
            self::COUNTERPART_COUNTRY_LA,
            self::COUNTERPART_COUNTRY_LB,
            self::COUNTERPART_COUNTRY_LC,
            self::COUNTERPART_COUNTRY_LI,
            self::COUNTERPART_COUNTRY_LK,
            self::COUNTERPART_COUNTRY_LR,
            self::COUNTERPART_COUNTRY_LS,
            self::COUNTERPART_COUNTRY_LT,
            self::COUNTERPART_COUNTRY_LU,
            self::COUNTERPART_COUNTRY_LV,
            self::COUNTERPART_COUNTRY_LY,
            self::COUNTERPART_COUNTRY_MA,
            self::COUNTERPART_COUNTRY_MC,
            self::COUNTERPART_COUNTRY_MD,
            self::COUNTERPART_COUNTRY_ME,
            self::COUNTERPART_COUNTRY_MF,
            self::COUNTERPART_COUNTRY_MG,
            self::COUNTERPART_COUNTRY_MH,
            self::COUNTERPART_COUNTRY_MK,
            self::COUNTERPART_COUNTRY_ML,
            self::COUNTERPART_COUNTRY_MM,
            self::COUNTERPART_COUNTRY_MN,
            self::COUNTERPART_COUNTRY_MO,
            self::COUNTERPART_COUNTRY_MP,
            self::COUNTERPART_COUNTRY_MQ,
            self::COUNTERPART_COUNTRY_MR,
            self::COUNTERPART_COUNTRY_MS,
            self::COUNTERPART_COUNTRY_MT,
            self::COUNTERPART_COUNTRY_MU,
            self::COUNTERPART_COUNTRY_MV,
            self::COUNTERPART_COUNTRY_MW,
            self::COUNTERPART_COUNTRY_MX,
            self::COUNTERPART_COUNTRY_MY,
            self::COUNTERPART_COUNTRY_MZ,
            self::COUNTERPART_COUNTRY_NA,
            self::COUNTERPART_COUNTRY_NC,
            self::COUNTERPART_COUNTRY_NE,
            self::COUNTERPART_COUNTRY_NF,
            self::COUNTERPART_COUNTRY_NG,
            self::COUNTERPART_COUNTRY_NI,
            self::COUNTERPART_COUNTRY_NL,
            self::COUNTERPART_COUNTRY_NO,
            self::COUNTERPART_COUNTRY_NP,
            self::COUNTERPART_COUNTRY_NR,
            self::COUNTERPART_COUNTRY_NU,
            self::COUNTERPART_COUNTRY_NZ,
            self::COUNTERPART_COUNTRY_OM,
            self::COUNTERPART_COUNTRY_PA,
            self::COUNTERPART_COUNTRY_PE,
            self::COUNTERPART_COUNTRY_PF,
            self::COUNTERPART_COUNTRY_PG,
            self::COUNTERPART_COUNTRY_PH,
            self::COUNTERPART_COUNTRY_PK,
            self::COUNTERPART_COUNTRY_PL,
            self::COUNTERPART_COUNTRY_PM,
            self::COUNTERPART_COUNTRY_PN,
            self::COUNTERPART_COUNTRY_PR,
            self::COUNTERPART_COUNTRY_PS,
            self::COUNTERPART_COUNTRY_PT,
            self::COUNTERPART_COUNTRY_PW,
            self::COUNTERPART_COUNTRY_PY,
            self::COUNTERPART_COUNTRY_QA,
            self::COUNTERPART_COUNTRY_RE,
            self::COUNTERPART_COUNTRY_RO,
            self::COUNTERPART_COUNTRY_RS,
            self::COUNTERPART_COUNTRY_RU,
            self::COUNTERPART_COUNTRY_RW,
            self::COUNTERPART_COUNTRY_SA,
            self::COUNTERPART_COUNTRY_SB,
            self::COUNTERPART_COUNTRY_SC,
            self::COUNTERPART_COUNTRY_SD,
            self::COUNTERPART_COUNTRY_SE,
            self::COUNTERPART_COUNTRY_SG,
            self::COUNTERPART_COUNTRY_SH,
            self::COUNTERPART_COUNTRY_SI,
            self::COUNTERPART_COUNTRY_SJ,
            self::COUNTERPART_COUNTRY_SK,
            self::COUNTERPART_COUNTRY_SL,
            self::COUNTERPART_COUNTRY_SM,
            self::COUNTERPART_COUNTRY_SN,
            self::COUNTERPART_COUNTRY_SO,
            self::COUNTERPART_COUNTRY_SR,
            self::COUNTERPART_COUNTRY_SS,
            self::COUNTERPART_COUNTRY_ST,
            self::COUNTERPART_COUNTRY_SV,
            self::COUNTERPART_COUNTRY_SX,
            self::COUNTERPART_COUNTRY_SY,
            self::COUNTERPART_COUNTRY_SZ,
            self::COUNTERPART_COUNTRY_TC,
            self::COUNTERPART_COUNTRY_TD,
            self::COUNTERPART_COUNTRY_TF,
            self::COUNTERPART_COUNTRY_TG,
            self::COUNTERPART_COUNTRY_TH,
            self::COUNTERPART_COUNTRY_TJ,
            self::COUNTERPART_COUNTRY_TK,
            self::COUNTERPART_COUNTRY_TL,
            self::COUNTERPART_COUNTRY_TM,
            self::COUNTERPART_COUNTRY_TN,
            self::COUNTERPART_COUNTRY_TO,
            self::COUNTERPART_COUNTRY_TR,
            self::COUNTERPART_COUNTRY_TT,
            self::COUNTERPART_COUNTRY_TV,
            self::COUNTERPART_COUNTRY_TW,
            self::COUNTERPART_COUNTRY_TZ,
            self::COUNTERPART_COUNTRY_UA,
            self::COUNTERPART_COUNTRY_UG,
            self::COUNTERPART_COUNTRY_UM,
            self::COUNTERPART_COUNTRY_US,
            self::COUNTERPART_COUNTRY_UY,
            self::COUNTERPART_COUNTRY_UZ,
            self::COUNTERPART_COUNTRY_VA,
            self::COUNTERPART_COUNTRY_VC,
            self::COUNTERPART_COUNTRY_VE,
            self::COUNTERPART_COUNTRY_VG,
            self::COUNTERPART_COUNTRY_VI,
            self::COUNTERPART_COUNTRY_VN,
            self::COUNTERPART_COUNTRY_VU,
            self::COUNTERPART_COUNTRY_WF,
            self::COUNTERPART_COUNTRY_WS,
            self::COUNTERPART_COUNTRY_XK,
            self::COUNTERPART_COUNTRY_YE,
            self::COUNTERPART_COUNTRY_YT,
            self::COUNTERPART_COUNTRY_ZA,
            self::COUNTERPART_COUNTRY_ZM,
            self::COUNTERPART_COUNTRY_ZW,
        ];
    }
    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['counterpart_name'] = isset($data['counterpart_name']) ? $data['counterpart_name'] : null;
        $this->container['counterpart_iban'] = isset($data['counterpart_iban']) ? $data['counterpart_iban'] : null;
        $this->container['counterpart_bic'] = isset($data['counterpart_bic']) ? $data['counterpart_bic'] : null;
        $this->container['amount'] = isset($data['amount']) ? $data['amount'] : null;
        $this->container['purpose'] = isset($data['purpose']) ? $data['purpose'] : null;
        $this->container['sepa_purpose_code'] = isset($data['sepa_purpose_code']) ? $data['sepa_purpose_code'] : null;
        $this->container['end_to_end_id'] = isset($data['end_to_end_id']) ? $data['end_to_end_id'] : null;
        $this->container['mandate_id'] = isset($data['mandate_id']) ? $data['mandate_id'] : null;
        $this->container['mandate_date'] = isset($data['mandate_date']) ? $data['mandate_date'] : null;
        $this->container['creditor_id'] = isset($data['creditor_id']) ? $data['creditor_id'] : null;
        $this->container['counterpart_address'] = isset($data['counterpart_address']) ? $data['counterpart_address'] : null;
        $this->container['counterpart_country'] = isset($data['counterpart_country']) ? $data['counterpart_country'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['counterpart_name'] === null) {
            $invalidProperties[] = "'counterpart_name' can't be null";
        }
        if ($this->container['counterpart_iban'] === null) {
            $invalidProperties[] = "'counterpart_iban' can't be null";
        }
        if ($this->container['amount'] === null) {
            $invalidProperties[] = "'amount' can't be null";
        }
        if ($this->container['mandate_id'] === null) {
            $invalidProperties[] = "'mandate_id' can't be null";
        }
        if ($this->container['mandate_date'] === null) {
            $invalidProperties[] = "'mandate_date' can't be null";
        }
        if ($this->container['creditor_id'] === null) {
            $invalidProperties[] = "'creditor_id' can't be null";
        }
        $allowedValues = $this->getCounterpartCountryAllowableValues();
        if (!is_null($this->container['counterpart_country']) && !in_array($this->container['counterpart_country'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'counterpart_country', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets counterpart_name
     *
     * @return string
     */
    public function getCounterpartName()
    {
        return $this->container['counterpart_name'];
    }

    /**
     * Sets counterpart_name
     *
     * @param string $counterpart_name Name of the counterpart. Note: Neither finAPI nor the involved bank servers are guaranteed to validate the counterpart name. Even if the name does not depict the actual registered account holder of the target account, the order might still be successful.
     *
     * @return $this
     */
    public function setCounterpartName($counterpart_name)
    {
        $this->container['counterpart_name'] = $counterpart_name;

        return $this;
    }

    /**
     * Gets counterpart_iban
     *
     * @return string
     */
    public function getCounterpartIban()
    {
        return $this->container['counterpart_iban'];
    }

    /**
     * Sets counterpart_iban
     *
     * @param string $counterpart_iban IBAN of the counterpart's account.
     *
     * @return $this
     */
    public function setCounterpartIban($counterpart_iban)
    {
        $this->container['counterpart_iban'] = $counterpart_iban;

        return $this;
    }

    /**
     * Gets counterpart_bic
     *
     * @return string
     */
    public function getCounterpartBic()
    {
        return $this->container['counterpart_bic'];
    }

    /**
     * Sets counterpart_bic
     *
     * @param string $counterpart_bic BIC of the counterpart's account. Only required when there is no 'IBAN_ONLY'-capability in the respective account/interface combination that is to be used when submitting the payment.
     *
     * @return $this
     */
    public function setCounterpartBic($counterpart_bic)
    {
        $this->container['counterpart_bic'] = $counterpart_bic;

        return $this;
    }

    /**
     * Gets amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->container['amount'];
    }

    /**
     * Sets amount
     *
     * @param float $amount The amount of the payment. Must be a positive decimal number with at most two decimal places. When debiting money using the FINTS_SERVER or WEB_SCRAPER interface, the currency is always EUR.
     *
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->container['amount'] = $amount;

        return $this;
    }

    /**
     * Gets purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->container['purpose'];
    }

    /**
     * Sets purpose
     *
     * @param string $purpose The purpose of the transfer transaction
     *
     * @return $this
     */
    public function setPurpose($purpose)
    {
        $this->container['purpose'] = $purpose;

        return $this;
    }

    /**
     * Gets sepa_purpose_code
     *
     * @return string
     */
    public function getSepaPurposeCode()
    {
        return $this->container['sepa_purpose_code'];
    }

    /**
     * Sets sepa_purpose_code
     *
     * @param string $sepa_purpose_code SEPA purpose code, according to ISO 20022, external codes set.<br/>Please note that the SEPA purpose code may be ignored by some banks.
     *
     * @return $this
     */
    public function setSepaPurposeCode($sepa_purpose_code)
    {
        $this->container['sepa_purpose_code'] = $sepa_purpose_code;

        return $this;
    }

    /**
     * Gets end_to_end_id
     *
     * @return string
     */
    public function getEndToEndId()
    {
        return $this->container['end_to_end_id'];
    }

    /**
     * Sets end_to_end_id
     *
     * @param string $end_to_end_id End-To-End ID for the transfer transaction
     *
     * @return $this
     */
    public function setEndToEndId($end_to_end_id)
    {
        $this->container['end_to_end_id'] = $end_to_end_id;

        return $this;
    }

    /**
     * Gets mandate_id
     *
     * @return string
     */
    public function getMandateId()
    {
        return $this->container['mandate_id'];
    }

    /**
     * Sets mandate_id
     *
     * @param string $mandate_id Mandate ID that this direct debit order is based on.
     *
     * @return $this
     */
    public function setMandateId($mandate_id)
    {
        $this->container['mandate_id'] = $mandate_id;

        return $this;
    }

    /**
     * Gets mandate_date
     *
     * @return string
     */
    public function getMandateDate()
    {
        return $this->container['mandate_date'];
    }

    /**
     * Sets mandate_date
     *
     * @param string $mandate_date Date of the mandate that this direct debit order is based on, in the format 'YYYY-MM-DD'
     *
     * @return $this
     */
    public function setMandateDate($mandate_date)
    {
        $this->container['mandate_date'] = $mandate_date;

        return $this;
    }

    /**
     * Gets creditor_id
     *
     * @return string
     */
    public function getCreditorId()
    {
        return $this->container['creditor_id'];
    }

    /**
     * Sets creditor_id
     *
     * @param string $creditor_id Creditor ID of the source account's holder
     *
     * @return $this
     */
    public function setCreditorId($creditor_id)
    {
        $this->container['creditor_id'] = $creditor_id;

        return $this;
    }

    /**
     * Gets counterpart_address
     *
     * @return string
     */
    public function getCounterpartAddress()
    {
        return $this->container['counterpart_address'];
    }

    /**
     * Sets counterpart_address
     *
     * @param string $counterpart_address The postal address of the debtor. This should be defined for direct debits created for debtors outside of the european union.
     *
     * @return $this
     */
    public function setCounterpartAddress($counterpart_address)
    {
        $this->container['counterpart_address'] = $counterpart_address;

        return $this;
    }

    /**
     * Gets counterpart_country
     *
     * @return string
     */
    public function getCounterpartCountry()
    {
        return $this->container['counterpart_country'];
    }

    /**
     * Sets counterpart_country
     *
     * @param string $counterpart_country The ISO 3166 ALPHA-2 country code of the debtor’s address. Examples: 'GB' for the United Kingdom or 'CH' for Switzerland. This should be defined for direct debits created for debtors outside of the european union.
     *
     * @return $this
     */
    public function setCounterpartCountry($counterpart_country)
    {
        $allowedValues = $this->getCounterpartCountryAllowableValues();
        if (!is_null($counterpart_country) && !in_array($counterpart_country, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'counterpart_country', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['counterpart_country'] = $counterpart_country;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}



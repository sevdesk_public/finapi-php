<?php
/**
 * Security
 *
 * PHP version 5
 *
 * @category Class
 * @package  Finapi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * finAPI Access
 *
 * <strong>RESTful API for Account Information Services (AIS) and Payment Initiation Services (PIS)</strong>  The following pages give you some general information on how to use our APIs.<br/> The actual API services documentation then follows further below. You can use the menu to jump between API sections. <br/> <br/> This page has a built-in HTTP(S) client, so you can test the services directly from within this page, by filling in the request parameters and/or body in the respective services, and then hitting the TRY button. Note that you need to be authorized to make a successful API call. To authorize, refer to the 'Authorization' section of the API, or just use the OAUTH button that can be found near the TRY button. <br/> <br/> You should also check out the <a href=\"https://finapi.zendesk.com/hc/en-us\" target=\"_blank\">Developer Portal</a> for more information. If you need any help with the API, contact support@finapi.io. <br/>  <h2>General information</h2>  <h3><strong>Error Responses</strong></h3> When an API call returns with an error, then in general it has the structure shown in the following example:  <pre> {   \"errors\": [     {       \"message\": \"Interface 'FINTS_SERVER' is not supported for this operation.\",       \"code\": \"BAD_REQUEST\",       \"type\": \"TECHNICAL\"     }   ],   \"date\": \"2020-11-19 16:54:06.854\",   \"requestId\": \"selfgen-312042e7-df55-47e4-bffd-956a68ef37b5\",   \"endpoint\": \"POST /api/v1/bankConnections/import\",   \"authContext\": \"1/21\",   \"bank\": \"DEMO0002 - finAPI Test Redirect Bank\" } </pre>  If an API call requires an additional authentication by the user, HTTP code 510 is returned and the error response contains the additional \"multiStepAuthentication\" object, see the following example:  <pre> {   \"errors\": [     {       \"message\": \"Es ist eine zusätzliche Authentifizierung erforderlich. Bitte geben Sie folgenden Code an: 123456\",       \"code\": \"ADDITIONAL_AUTHENTICATION_REQUIRED\",       \"type\": \"BUSINESS\",       \"multiStepAuthentication\": {         \"hash\": \"678b13f4be9ed7d981a840af8131223a\",         \"status\": \"CHALLENGE_RESPONSE_REQUIRED\",         \"challengeMessage\": \"Es ist eine zusätzliche Authentifizierung erforderlich. Bitte geben Sie folgenden Code an: 123456\",         \"answerFieldLabel\": \"TAN\",         \"redirectUrl\": null,         \"redirectContext\": null,         \"redirectContextField\": null,         \"twoStepProcedures\": null,         \"photoTanMimeType\": null,         \"photoTanData\": null,         \"opticalData\": null       }     }   ],   \"date\": \"2019-11-29 09:51:55.931\",   \"requestId\": \"selfgen-45059c99-1b14-4df7-9bd3-9d5f126df294\",   \"endpoint\": \"POST /api/v1/bankConnections/import\",   \"authContext\": \"1/18\",   \"bank\": \"DEMO0001 - finAPI Test Bank\" } </pre>  An exception to this error format are API authentication errors, where the following structure is returned:  <pre> {   \"error\": \"invalid_token\",   \"error_description\": \"Invalid access token: cccbce46-xxxx-xxxx-xxxx-xxxxxxxxxx\" } </pre>  <h3><strong>Paging</strong></h3> API services that may potentially return a lot of data implement paging. They return a limited number of entries within a \"page\". Further entries must be fetched with subsequent calls. <br/><br/> Any API service that implements paging provides the following input parameters:<br/> &bull; \"page\": the number of the page to be retrieved (starting with 1).<br/> &bull; \"perPage\": the number of entries within a page. The default and maximum value is stated in the documentation of the respective services.  A paged response contains an additional \"paging\" object with the following structure:  <pre> {   ...   ,   \"paging\": {     \"page\": 1,     \"perPage\": 20,     \"pageCount\": 234,     \"totalCount\": 4662   } } </pre>  <h3><strong>Internationalization</strong></h3> The finAPI services support internationalization which means you can define the language you prefer for API service responses. <br/><br/> The following languages are available: German, English, Czech, Slovak. <br/><br/> The preferred language can be defined by providing the official HTTP <strong>Accept-Language</strong> header. For web form request issued in a web browser, the Accept-Language header is automatically set by the browser based on the browser's or operation system's language settings. For direct API calls, the Accept-Language header must be set explicity. <br/><br/> finAPI reacts on the official iso language codes &quot;de&quot;, &quot;en&quot;, &quot;cs&quot; and &quot;sk&quot; for the named languages. Additional subtags supported by the Accept-Language header may be provided, e.g. &quot;en-US&quot;, but are ignored. <br/> If no Accept-Language header is given, German is used as the default language. <br/><br/> Exceptions:<br/> &bull; Bank login hints and login fields are only available in the language of the bank and not being translated.<br/> &bull; Direct messages from the bank systems typically returned as BUSINESS errors will not be translated.<br/> &bull; BUSINESS errors created by finAPI directly are available in German and English.<br/> &bull; TECHNICAL errors messages meant for developers are mostly in English, but also may be translated.  <h3><strong>Request IDs</strong></h3> With any API call, you can pass a request ID via a header with name \"X-REQUEST-ID\". The request ID can be an arbitrary string with up to 255 characters. Passing a longer string will result in an error. <br/><br/> If you don't pass a request ID for a call, finAPI will generate a random ID internally. <br/><br/> The request ID is always returned back in the response of a service, as a header with name \"X-REQUEST-ID\". <br/><br/> We highly recommend to always pass a (preferably unique) request ID, and include it into your client application logs whenever you make a request or receive a response (especially in the case of an error response). finAPI is also logging request IDs on its end. Having a request ID can help the finAPI support team to work more efficiently and solve tickets faster.  <h3><strong>Overriding HTTP methods</strong></h3> Some HTTP clients do not support the HTTP methods PATCH or DELETE. If you are using such a client in your application, you can use a POST request instead with a special HTTP header indicating the originally intended HTTP method. <br/><br/> The header's name is <strong>X-HTTP-Method-Override</strong>. Set its value to either <strong>PATCH</strong> or <strong>DELETE</strong>. POST Requests having this header set will be treated either as PATCH or DELETE by the finAPI servers. <br/><br/> Example: <br/><br/> <strong>X-HTTP-Method-Override: PATCH</strong><br/> POST /api/v1/label/51<br/> {\"name\": \"changed label\"}<br/><br/> will be interpreted by finAPI as:<br/><br/> PATCH /api/v1/label/51<br/> {\"name\": \"changed label\"}<br/>  <h3><strong>User metadata</strong></h3> With the migration to PSD2 APIs, a new term called \"User metadata\" (also known as \"PSU metadata\") has been introduced to the API. This user metadata aims to inform the banking API if there was a real end-user behind an HTTP request or if the request was triggered by a system (e.g. by an automatic batch update). In the latter case, the bank may apply some restrictions such as limiting the number of HTTP requests for a single consent. Also, some operations may be forbidden entirely by the banking API. For example, some banks do not allow issuing a new consent without the end-user being involved. Therefore, the PSU metadata must always be provided for such operations. <br/><br/> As finAPI does not have direct interaction with the end-user, it is the client application's responsibility to provide all the necessary information about the end-user. This must be done by sending additional headers with every request triggered on behalf of the end-user. <br/><br/> At the moment, the following headers are supported by the API:<br/> &bull; \"PSU-IP-Address\" - the IP address of the user's device.<br/> &bull; \"PSU-Device-OS\" - the user's device and/or operating system identification.<br/> &bull; \"PSU-User-Agent\" - the user's web browser or other client device identification. <br/><br/> Web-form customers (or unlicensed customers) must send the PSU headers from their client application to finAPI. It will not take effect if web form is triggered for the workflow. <br/> In this case Values for the PSU-Device-OS and PSU-User-Agent headers are identified by the JS platform detection and the PSU-IP-Address is obtained from a public Cloudflare service: https://www.cloudflare.com/cdn-cgi/trace. <br/><br/> But it is certainly necessary and obligatory to have the true PSU header data for API calls which don't trigger a web form (like \"Update a bank connection\").  <h3><strong>FAQ</strong></h3> <strong>Is there a finAPI SDK?</strong> <br/> Currently we do not offer a native SDK, but there is the option to generate a SDK for almost any target language via Swagger. Use the 'Download SDK' button on this page for SDK generation. <br/> <br/> <strong>How can I enable finAPI's automatic batch update?</strong> <br/> Currently there is no way to set up the batch update via the API. Please contact support@finapi.io for this. <br/> <br/> <strong>Why do I need to keep authorizing when calling services on this page?</strong> <br/> This page is a \"one-page-app\". Reloading the page resets the OAuth authorization context. There is generally no need to reload the page, so just don't do it and your authorization will persist.
 *
 * OpenAPI spec version: 1.121.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.18
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Finapi\Model;

use \ArrayAccess;
use \Finapi\ObjectSerializer;

/**
 * Security Class Doc Comment
 *
 * @category Class
 * @description Container for a security position&#39;s data
 * @package  Finapi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Security implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'Security';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
        'account_id' => 'int',
        'name' => 'string',
        'isin' => 'string',
        'wkn' => 'string',
        'quote' => 'float',
        'quote_currency' => 'string',
        'quote_type' => 'string',
        'quote_date' => 'string',
        'quantity_nominal' => 'float',
        'quantity_nominal_type' => 'string',
        'market_value' => 'float',
        'market_value_currency' => 'string',
        'entry_quote' => 'float',
        'entry_quote_currency' => 'string',
        'profit_or_loss' => 'float'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => 'int64',
        'account_id' => 'int64',
        'name' => null,
        'isin' => null,
        'wkn' => null,
        'quote' => null,
        'quote_currency' => null,
        'quote_type' => null,
        'quote_date' => null,
        'quantity_nominal' => null,
        'quantity_nominal_type' => null,
        'market_value' => null,
        'market_value_currency' => null,
        'entry_quote' => null,
        'entry_quote_currency' => null,
        'profit_or_loss' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'account_id' => 'accountId',
        'name' => 'name',
        'isin' => 'isin',
        'wkn' => 'wkn',
        'quote' => 'quote',
        'quote_currency' => 'quoteCurrency',
        'quote_type' => 'quoteType',
        'quote_date' => 'quoteDate',
        'quantity_nominal' => 'quantityNominal',
        'quantity_nominal_type' => 'quantityNominalType',
        'market_value' => 'marketValue',
        'market_value_currency' => 'marketValueCurrency',
        'entry_quote' => 'entryQuote',
        'entry_quote_currency' => 'entryQuoteCurrency',
        'profit_or_loss' => 'profitOrLoss'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'account_id' => 'setAccountId',
        'name' => 'setName',
        'isin' => 'setIsin',
        'wkn' => 'setWkn',
        'quote' => 'setQuote',
        'quote_currency' => 'setQuoteCurrency',
        'quote_type' => 'setQuoteType',
        'quote_date' => 'setQuoteDate',
        'quantity_nominal' => 'setQuantityNominal',
        'quantity_nominal_type' => 'setQuantityNominalType',
        'market_value' => 'setMarketValue',
        'market_value_currency' => 'setMarketValueCurrency',
        'entry_quote' => 'setEntryQuote',
        'entry_quote_currency' => 'setEntryQuoteCurrency',
        'profit_or_loss' => 'setProfitOrLoss'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'account_id' => 'getAccountId',
        'name' => 'getName',
        'isin' => 'getIsin',
        'wkn' => 'getWkn',
        'quote' => 'getQuote',
        'quote_currency' => 'getQuoteCurrency',
        'quote_type' => 'getQuoteType',
        'quote_date' => 'getQuoteDate',
        'quantity_nominal' => 'getQuantityNominal',
        'quantity_nominal_type' => 'getQuantityNominalType',
        'market_value' => 'getMarketValue',
        'market_value_currency' => 'getMarketValueCurrency',
        'entry_quote' => 'getEntryQuote',
        'entry_quote_currency' => 'getEntryQuoteCurrency',
        'profit_or_loss' => 'getProfitOrLoss'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const QUOTE_TYPE_ACTU = 'ACTU';
    const QUOTE_TYPE_PERC = 'PERC';
    const QUANTITY_NOMINAL_TYPE_UNIT = 'UNIT';
    const QUANTITY_NOMINAL_TYPE_FAMT = 'FAMT';
    

    
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getQuoteTypeAllowableValues()
    {
        return [
            self::QUOTE_TYPE_ACTU,
            self::QUOTE_TYPE_PERC,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getQuantityNominalTypeAllowableValues()
    {
        return [
            self::QUANTITY_NOMINAL_TYPE_UNIT,
            self::QUANTITY_NOMINAL_TYPE_FAMT,
        ];
    }
    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['account_id'] = isset($data['account_id']) ? $data['account_id'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['isin'] = isset($data['isin']) ? $data['isin'] : null;
        $this->container['wkn'] = isset($data['wkn']) ? $data['wkn'] : null;
        $this->container['quote'] = isset($data['quote']) ? $data['quote'] : null;
        $this->container['quote_currency'] = isset($data['quote_currency']) ? $data['quote_currency'] : null;
        $this->container['quote_type'] = isset($data['quote_type']) ? $data['quote_type'] : null;
        $this->container['quote_date'] = isset($data['quote_date']) ? $data['quote_date'] : null;
        $this->container['quantity_nominal'] = isset($data['quantity_nominal']) ? $data['quantity_nominal'] : null;
        $this->container['quantity_nominal_type'] = isset($data['quantity_nominal_type']) ? $data['quantity_nominal_type'] : null;
        $this->container['market_value'] = isset($data['market_value']) ? $data['market_value'] : null;
        $this->container['market_value_currency'] = isset($data['market_value_currency']) ? $data['market_value_currency'] : null;
        $this->container['entry_quote'] = isset($data['entry_quote']) ? $data['entry_quote'] : null;
        $this->container['entry_quote_currency'] = isset($data['entry_quote_currency']) ? $data['entry_quote_currency'] : null;
        $this->container['profit_or_loss'] = isset($data['profit_or_loss']) ? $data['profit_or_loss'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['id'] === null) {
            $invalidProperties[] = "'id' can't be null";
        }
        if ($this->container['account_id'] === null) {
            $invalidProperties[] = "'account_id' can't be null";
        }
        $allowedValues = $this->getQuoteTypeAllowableValues();
        if (!is_null($this->container['quote_type']) && !in_array($this->container['quote_type'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'quote_type', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        $allowedValues = $this->getQuantityNominalTypeAllowableValues();
        if (!is_null($this->container['quantity_nominal_type']) && !in_array($this->container['quantity_nominal_type'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'quantity_nominal_type', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id Identifier. Note: Whenever a security account is being updated, its security positions will be internally re-created, meaning that the identifier of a security position might change over time.
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets account_id
     *
     * @return int
     */
    public function getAccountId()
    {
        return $this->container['account_id'];
    }

    /**
     * Sets account_id
     *
     * @param int $account_id Security account identifier
     *
     * @return $this
     */
    public function setAccountId($account_id)
    {
        $this->container['account_id'] = $account_id;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name Name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets isin
     *
     * @return string
     */
    public function getIsin()
    {
        return $this->container['isin'];
    }

    /**
     * Sets isin
     *
     * @param string $isin ISIN
     *
     * @return $this
     */
    public function setIsin($isin)
    {
        $this->container['isin'] = $isin;

        return $this;
    }

    /**
     * Gets wkn
     *
     * @return string
     */
    public function getWkn()
    {
        return $this->container['wkn'];
    }

    /**
     * Sets wkn
     *
     * @param string $wkn WKN
     *
     * @return $this
     */
    public function setWkn($wkn)
    {
        $this->container['wkn'] = $wkn;

        return $this;
    }

    /**
     * Gets quote
     *
     * @return float
     */
    public function getQuote()
    {
        return $this->container['quote'];
    }

    /**
     * Sets quote
     *
     * @param float $quote Quote
     *
     * @return $this
     */
    public function setQuote($quote)
    {
        $this->container['quote'] = $quote;

        return $this;
    }

    /**
     * Gets quote_currency
     *
     * @return string
     */
    public function getQuoteCurrency()
    {
        return $this->container['quote_currency'];
    }

    /**
     * Sets quote_currency
     *
     * @param string $quote_currency Currency of quote
     *
     * @return $this
     */
    public function setQuoteCurrency($quote_currency)
    {
        $this->container['quote_currency'] = $quote_currency;

        return $this;
    }

    /**
     * Gets quote_type
     *
     * @return string
     */
    public function getQuoteType()
    {
        return $this->container['quote_type'];
    }

    /**
     * Sets quote_type
     *
     * @param string $quote_type Type of quote. 'PERC' if quote is a percentage value, 'ACTU' if quote is the actual amount
     *
     * @return $this
     */
    public function setQuoteType($quote_type)
    {
        $allowedValues = $this->getQuoteTypeAllowableValues();
        if (!is_null($quote_type) && !in_array($quote_type, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'quote_type', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['quote_type'] = $quote_type;

        return $this;
    }

    /**
     * Gets quote_date
     *
     * @return string
     */
    public function getQuoteDate()
    {
        return $this->container['quote_date'];
    }

    /**
     * Sets quote_date
     *
     * @param string $quote_date Quote date in the format 'YYYY-MM-DD HH:MM:SS.SSS' (german time).
     *
     * @return $this
     */
    public function setQuoteDate($quote_date)
    {
        $this->container['quote_date'] = $quote_date;

        return $this;
    }

    /**
     * Gets quantity_nominal
     *
     * @return float
     */
    public function getQuantityNominal()
    {
        return $this->container['quantity_nominal'];
    }

    /**
     * Sets quantity_nominal
     *
     * @param float $quantity_nominal Value of quantity or nominal
     *
     * @return $this
     */
    public function setQuantityNominal($quantity_nominal)
    {
        $this->container['quantity_nominal'] = $quantity_nominal;

        return $this;
    }

    /**
     * Gets quantity_nominal_type
     *
     * @return string
     */
    public function getQuantityNominalType()
    {
        return $this->container['quantity_nominal_type'];
    }

    /**
     * Sets quantity_nominal_type
     *
     * @param string $quantity_nominal_type Type of quantity or nominal value. 'UNIT' if value is a quantity, 'FAMT' if value is the nominal amount
     *
     * @return $this
     */
    public function setQuantityNominalType($quantity_nominal_type)
    {
        $allowedValues = $this->getQuantityNominalTypeAllowableValues();
        if (!is_null($quantity_nominal_type) && !in_array($quantity_nominal_type, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'quantity_nominal_type', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['quantity_nominal_type'] = $quantity_nominal_type;

        return $this;
    }

    /**
     * Gets market_value
     *
     * @return float
     */
    public function getMarketValue()
    {
        return $this->container['market_value'];
    }

    /**
     * Sets market_value
     *
     * @param float $market_value Market value
     *
     * @return $this
     */
    public function setMarketValue($market_value)
    {
        $this->container['market_value'] = $market_value;

        return $this;
    }

    /**
     * Gets market_value_currency
     *
     * @return string
     */
    public function getMarketValueCurrency()
    {
        return $this->container['market_value_currency'];
    }

    /**
     * Sets market_value_currency
     *
     * @param string $market_value_currency Currency of market value
     *
     * @return $this
     */
    public function setMarketValueCurrency($market_value_currency)
    {
        $this->container['market_value_currency'] = $market_value_currency;

        return $this;
    }

    /**
     * Gets entry_quote
     *
     * @return float
     */
    public function getEntryQuote()
    {
        return $this->container['entry_quote'];
    }

    /**
     * Sets entry_quote
     *
     * @param float $entry_quote Entry quote
     *
     * @return $this
     */
    public function setEntryQuote($entry_quote)
    {
        $this->container['entry_quote'] = $entry_quote;

        return $this;
    }

    /**
     * Gets entry_quote_currency
     *
     * @return string
     */
    public function getEntryQuoteCurrency()
    {
        return $this->container['entry_quote_currency'];
    }

    /**
     * Sets entry_quote_currency
     *
     * @param string $entry_quote_currency Currency of entry quote
     *
     * @return $this
     */
    public function setEntryQuoteCurrency($entry_quote_currency)
    {
        $this->container['entry_quote_currency'] = $entry_quote_currency;

        return $this;
    }

    /**
     * Gets profit_or_loss
     *
     * @return float
     */
    public function getProfitOrLoss()
    {
        return $this->container['profit_or_loss'];
    }

    /**
     * Sets profit_or_loss
     *
     * @param float $profit_or_loss Current profit or loss
     *
     * @return $this
     */
    public function setProfitOrLoss($profit_or_loss)
    {
        $this->container['profit_or_loss'] = $profit_or_loss;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


